class Post < ApplicationRecord

	belongs_to :category
	has_many :comments, dependent: :destroy
	has_one_attached :image
	has_many :taggings, dependent: :destroy
	has_many :tags, through: :taggings, dependent: :destroy
	has_rich_text :content

	validates :title, presence: true
	validates :body, presence: true, length: {minimum: 10, maximum: 300}
	validates :content, presence: true, length: {minimum: 100}

	def all_tags
		self.tags.map(&:name).join(', ')
	end

	def all_tags=(names)
		self.tags = names.split(',').map do |name|
			Tag.where(name: name.strip).first_or_create!
		end
	end
end
