module PostsHelper

	def tags_navigation(tags, css)
		tags.each do |tag|
			yield(tag, css)
		end
	end
end