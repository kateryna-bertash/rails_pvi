module ApplicationHelper

	def build_breadcrumbs(category)
		result = []
		category.ancestors.each do |ancestor|
			result << content_tag(:li, class: "breadcrumb-item") do
				link_to(ancestor.name, ancestor)
			end
		end
		result << content_tag(:li, category.name, class: "breadcrumb-item active")
		result.join('').html_safe
	end

	def is_user_admin?
		if current_user.admin
			return true
		else
			return false
		end
	end
end
