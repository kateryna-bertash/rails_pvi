class Admin::CategoriesController < ApplicationController
	before_action :set_category, only: [:edit, :update, :destroy]
	before_action :authenticate_user!, except: :show

	def index
		@categories = Category.all
	end

	def new
		@category = Category.new
		@categories = Category.all.order(:name)
	end

	def create
		@category = Category.new(category_params)
		
		if @category.save
			redirect_to admin_categories_path
		else
			render :new
		end 
	end

	def edit
		@categories = Category.where("id != #{@category.id}").order(:name)
	end

	def update
		if @category.update(category_params)
			redirect_to admin_categories_path
		else
			render :edit
		end
	end

	def destroy
		@category.destroy
		redirect_to admin_categories_path
	end

	private

		def set_category
			@category = Category.find(params[:id])
		end

		def category_params
			params.require(:category).permit(:name, :parent_id)
		end
end
