class TagsController < ApplicationController
	
	def index
		@tags = Tag.all
	end

	def show
		@tag = Tag.find_by(name: params[:id])
		@posts = @tag.posts.paginate(:page => params[:page], :per_page => 5)
	end
end