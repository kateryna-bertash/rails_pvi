class PostsController < ApplicationController

	before_action :authenticate_user!, except: [:index, :show]

	def index
		@posts = Post.paginate(page: params[:page], per_page: 5)
	end

	def show
		@post = Post.find(params[:id])
	end

	def new
		@post = Post.new
	end

	def create
		@post = Post.new(post_params)

		if @post.save
			redirect_to @post
		else
			render :new, status: :unprocessable_entity
		end
	end

	def edit
		@post = Post.find(params[:id])
	end

	def update
		@post = Post.find(params[:id])

		if @post.update(post_params)
			redirect_to @post
		else
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		@post = Post.find(params[:id])
		@post.destroy

		redirect_to root_path
	end

	def removeImage
		@post = Post.find(params[:id])

		if @post.image.attached?
			@post.image.purge_later
		end

		redirect_to edit_post_path
	end

	def search
		@posts = Post.where("title LIKE ?", "%" + params[:q] + "%")
	end

	private
		def post_params
			params.require(:post).permit(:title, :body, :image, :all_tags, :category_id, :content)
		end
end
