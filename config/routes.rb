Rails.application.routes.draw do

	devise_for :users
	root "posts#index"
	resources :tags, only: [:show]
	resources :categories, only: [:show]
	resources :posts do
		member do
			delete :removeImage
		end
		resources :comments
	end

	get 'search', to: "posts#search"

	namespace :admin do
		resources :categories, except: [:show]
	end
end
