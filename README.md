# Контрольна робота студентки ПЗ-21 Берташ Катерини: 

## Приватний блог

### Реалізовані фічі:
- getting started
- пагінація (на кожній сторінці відображається максимум 5 постів )
- фільтрування за тегами
- фільтрування за категоріями (+вкладені категорії по типу Programming->Ruby->Rails, Programing->C++)
- пошук (за назвою поста: будь-яким словом/символом із назви)
- завантаження картинок: аватарка поста + картинки як контент поста
- авторизація (див. пункт 'Права доступу') + режим адміна
- базова автентифікація різними користувачами (з використанням gem devise)
- коменти (disqus comments)
- підключено trix editor

#### Права доступу
- Адмін може: створювати нові пости, видаляти/редагувати старі, додавати/редагувати/видаляти категорії
- Зареєстрований користувач може: дивитись всі пости, читати кожен пост окремо, писати коменти
- Гість може: дивитись список постів (назви і короткий опис), фільтрувати їх, здійснювати пошук, НЕ може дивитись кожен пост окремо

#### Нотатки:
- Щоб швидко створити акаунт адміна треба зробити наступне:
```bash
cd bin
rails console
user = User.new(:id => "1", :email => "admin@gmail.com", :password => "password", :admin => "true")
user.save
```
- Щоб блог запрацював потрібно буде скачати action_text:
```bash
cd bin
rails action_text:install
```
- Пост без категорій створити неможливо, тому спочатку потрібно перейти на сторінку редагування категорій і додати їх, а тоді вже створювати пости

#### Джерела
- Список відтворення на ютубі про створення блогу: https://youtube.com/playlist?list=PL0ZEIs68t9d1qh6Z92-E45R8YBPvgy4wk
- Відео на ютубі про роботу з action_text: https://www.youtube.com/watch?v=7_ScfESnIjk
- Відео на ютубі про роботу з active_storage: https://www.youtube.com/watch?v=fVtGy3QL9xg&list=PL3mtAHT_eRexVRWcjcqgfTvYQD2scoTlk
- Підключала коменти за допомогою цього сайту: https://disqus.com/
- Підключала trix editor за допомогою цього сайту: https://edgeguides.rubyonrails.org/action_text_overview.html
- Для шаблону стилізації сторінки використовувала ось це: https://getbootstrap.com/docs/4.0/examples/
- Геттінг стартед робила за цим: https://guides.rubyonrails.org/getting_started.html

#### Скріни

![Main-Page](res/main-page.jpg)
![Single-Post-Page](res/single-post-page.jpg)
![Categories-Page](res/categories-page.jpg)
